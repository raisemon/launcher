package pl.raisemon.launcher.api;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginResponse {
    private final String token_type;
    private final int expires_in;
    private final String access_token;
    private final String refresh_token;
    private final String name;
}
