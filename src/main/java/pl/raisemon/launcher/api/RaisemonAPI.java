package pl.raisemon.launcher.api;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.json.JSONObject;
import pl.raisemon.launcher.Launcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RaisemonAPI {
    private static String BASE_URL = "https://raisemon.pl/api";


    public static LoginResponse login(String login, String password) {


        HttpResponse<JsonNode> stringHttpResponse = null;
        try {
            stringHttpResponse = Unirest.post(BASE_URL + "/login")
                    .header("accept", "application/json")
                    .field("name", login)
                    .field("password", password)
                    .field("app", "game").asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
            Launcher.getSentry().sendException(e);
            return null;
        }
        if(stringHttpResponse.getStatus() == 200){

            final JSONObject jsonObject = stringHttpResponse.getBody().getObject();
            return new LoginResponse(
                    jsonObject.getString("token_type"),
                    jsonObject.getInt("expires_in"),
                    jsonObject.getString("access_token"),
                    jsonObject.getString("refresh_token"),
                    login
            );
        }
        return null;
    }

    public static RegisterResponse register(String nickname, String password, String passwordRepeat) {

        HttpResponse<String> stringHttpResponse = null;
        try {
            stringHttpResponse = Unirest.post(BASE_URL + "/register")
                    .header("accept", "application/json")
                    .field("name", nickname)
                    .field("password", password)
                    .field("c_password", passwordRepeat).asString();
        } catch (UnirestException e) {
            e.printStackTrace();
            Launcher.getSentry().sendException(e);
            return null;
        }
        final JsonObject jsonObject = Launcher.GSON.fromJson(stringHttpResponse.getBody(), JsonObject.class);

        if (stringHttpResponse.getStatus() == 200) {
            return new RegisterResponse(true, null);
        }
        final ArrayList<String> objects = new ArrayList<>();
        for (Map.Entry<String, JsonElement> error : jsonObject.get("error").getAsJsonObject().entrySet()) {
            for (JsonElement jsonElement : error.getValue().getAsJsonArray()) {
                objects.add(jsonElement.getAsString());
            }
        }
        return new RegisterResponse(false, objects);
    }

    public static boolean logout(String token) {
        HttpResponse<String> response = null;
        try {
            response = Unirest.post(BASE_URL + "/logout")
                    .header("accept", "application/json")
                    .header("Authorization", "Bearer " + token)
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
            Launcher.getSentry().sendException(e);
            return false;
        }
        return response != null && response.getStatus() == 200;
    }

    public static LoginResponse refreshToken(String refreshToken) {


        HttpResponse<JsonNode> stringHttpResponse = null;
        try {
            stringHttpResponse = Unirest.post(BASE_URL + "/refresh")
                    .header("accept", "application/json")
                    .field("token", refreshToken)
                    .field("app", "game").asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
            Launcher.getSentry().sendException(e);
            return null;
        }
        if(stringHttpResponse.getStatus() == 200){
            final JSONObject jsonObject = stringHttpResponse.getBody().getObject();
            HttpResponse<JsonNode> details = null;
            try {
                details = Unirest.post(BASE_URL + "/details")
                        .header("accept", "application/json")
                        .header("Authorization", "Bearer " + jsonObject.getString("access_token"))
                        .asJson();
            } catch (UnirestException e) {
                e.printStackTrace();
                Launcher.getSentry().sendException(e);
                return null;
            }

            return new LoginResponse(
                    jsonObject.getString("token_type"),
                    jsonObject.getInt("expires_in"),
                    jsonObject.getString("access_token"),
                    jsonObject.getString("refresh_token"),
                    details.getBody().getObject().getString("name")
            );
        }
        return null;
    }

    @Data
    @AllArgsConstructor
    public static class RegisterResponse {
        private boolean success;
        private List<String> errors;
    }
}
