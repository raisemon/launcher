package pl.raisemon.launcher.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.raisemon.launcher.Launcher;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class Config {
    private File file;
    @Getter
    private Settings settings;


    public Config(File file) {
        this.file = file;
        if(!file.exists()){
            this.settings = new Settings();
            return;
        }
        try {
            this.settings = Launcher.GSON.fromJson(new FileReader(file), Settings.class);
        } catch (FileNotFoundException e) {
            this.settings = new Settings();
            e.printStackTrace();
        }
    }

    public void save(){
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(Launcher.GSON.toJson(this.settings));
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Data
    @NoArgsConstructor
    public static class Settings{
        private int ram;
        private boolean lowProfile;
        private LoginTokenData tokenData;
        private LinkedHashSet<String> optionalMods = new LinkedHashSet<>();
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LoginTokenData {
        private String refreshToken;
        private String name;
    }
}
