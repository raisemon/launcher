package pl.raisemon.launcher;

import javafx.application.Platform;
import lombok.Setter;
import pl.raisemon.launcher.data.MinecraftVersion;
import pl.raisemon.launcher.utils.Utils;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class MinecraftLauncher {
    private MinecraftVersion version;
    @Setter
    private int ram;
    @Setter
    private String token;
    @Setter
    private String name;
    private ServerSocket serverSocket;
    private Thread thread;

    public MinecraftLauncher(MinecraftVersion version) {
        this.version = version;
    }

    public void launch() {
        try {
            serverSocket = new ServerSocket(0, 50, InetAddress.getLocalHost());
        } catch (IOException e) {
            e.printStackTrace();
        }
        long t = System.currentTimeMillis();
        while (!serverSocket.isBound()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (System.currentTimeMillis() - t > TimeUnit.SECONDS.toMillis(10)) {
                System.out.println("ERROR WHILE CREATING BINDING SERVERSOCKET");
            }
        }
        thread = new Thread(() -> {
            try {
                System.out.println("waiting for socket " + serverSocket.getLocalPort());
                Socket accept = serverSocket.accept();
                System.out.println("connection established");
                DataOutputStream dataOutputStream = new DataOutputStream(accept.getOutputStream());
                byte[] bytes = token.getBytes(StandardCharsets.UTF_8);
                dataOutputStream.writeShort(bytes.length);
                dataOutputStream.write(bytes);
                dataOutputStream.flush();
                dataOutputStream.close();
                System.out.println("token sent");
                accept.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                Launcher.getInstance().getPrimaryStage().close();
                //Platform.exit();
            });

        });
        thread.setName("TOKEN SOCKET");
        thread.setDaemon(false);
        thread.start();

        String args = "java -javaagent:agent.jar=" + serverSocket.getLocalPort() +
                " -Xmx" + ram + "M -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy " +
                "-Djava.library.path=natives/ " +
                "-Dminecraft.launcher.brand=java-minecraft-raisemon " +
                "-Dminecraft.launcher.version=1.0 " +
                "-Dminecraft.client.jar=version.jar" +
                " -cp " + getLibraryPath() + " " +
                version.getMainClass() + " " +
                version.getMinecraftArguments()
                        .replace("${auth_player_name}", name)
                        .replace("${version_name}", version.getJar())

                        .replace("${game_directory}",
                                ((Utils.getOperatingSystem().equals("linux") || Utils.getOperatingSystem().equals("osx")) ? "." : ("\"" + Launcher.getInstance().getLauncherDir().getAbsolutePath() + "\"")))
                        //.replace("${game_directory}", ".")
                        .replace("${assets_root}", "assets/")
                        .replace("${assets_index_name}", version.getAssetIndex().getId())
                        .replace("${auth_uuid}", "{}")
                        .replace("${auth_access_token}", "{}")
                        .replace("${user_type}", "").replace("\r", "").replace("\n", "");
        System.out.println("Args " + args);
        String[] split = args.split(" ");
        ProcessBuilder processBuilder = new ProcessBuilder(Arrays.asList(split));
        processBuilder.directory(Launcher.getInstance().getLauncherDir());
        try {
            System.out.println("Launching");
            processBuilder.inheritIO();
            Process start = processBuilder.start();

            Platform.runLater(() -> Launcher.getInstance().getPrimaryStage().hide());
        } catch (IOException e) {
            Launcher.getSentry().sendException(e);
            //Launcher.getInstance().getLogger().error("Exception caught", e);
        }
        new Thread(()->{
            try {
                Thread.sleep(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Platform.exit();
        }).start();
    }

    public String getLibraryPath() {
        String libSeparator = System.getProperty("path.separator");
        StringBuilder path = new StringBuilder();
        for (MinecraftVersion.MinecraftLibrary library : version.getLibraries()) {

            if (library.getExtract() != null) {
                final String s = library.getNatives().get(Utils.getOperatingSystem());
                if (s != null && library.shouldUse()) {
                    Utils.unzipLibrary(library.getExtract(), library.getDownloads().getClassifiers().get(s));
                }
                continue;
            }
            if (library.getDownloads() != null && library.getDownloads().getArtifact() != null) {
                final MinecraftVersion.MinecraftLibrary.LibraryDownload artifact = library.getDownloads().getArtifact();
                File file = new File(Launcher.getInstance().getLauncherDir(), "libraries/" + artifact.getPath());
                if (!file.exists()) {
                    file = null;
                    continue;
                }
                path.append("libraries/" + artifact.getPath() + libSeparator);
                //path.append(file.getAbsolutePath() + libSeparator);
            }
            if (library.getNatives() != null) {
                continue;
            }
            String[] data = library.getName().split(":");
            String filename = data[1] + "-" + data[2] + ".jar";

            File file = new File(Launcher.getInstance().getLauncherDir(), "libraries/" + data[0].replace(".", "/") + "/" + data[1] + "/" + data[2] + "/" + filename);
            if (!file.exists()) {
                file = null;
                continue;
            }
            path.append("libraries/" + data[0].replace(".", "/") + "/" + data[1] + "/" + data[2] + "/" + filename + libSeparator);
            // path.append(file.getAbsolutePath() + libSeparator);
        }
        return path.append("version.jar").toString();
    }
}
