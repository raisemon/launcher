package pl.raisemon.launcher;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.TextAlignment;
import lombok.Getter;
import org.controlsfx.control.CheckComboBox;
import org.controlsfx.control.ToggleSwitch;
import pl.raisemon.launcher.api.LoginResponse;
import pl.raisemon.launcher.api.RaisemonAPI;
import pl.raisemon.launcher.data.Config;
import pl.raisemon.launcher.data.RaisemonRepo;
import pl.raisemon.launcher.utils.EffectUtilities;

import java.net.URL;
import java.util.Arrays;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static pl.raisemon.launcher.Launcher.GSON;

public class LauncherController implements Initializable {
    @FXML
    public Label statusLabel;
    @FXML
    public Button playButton;
    @FXML
    public ToggleSwitch savePasswordCheckBox;
    @FXML
    public ProgressBar progressBar;
    @FXML
    public TextField nicknameField;
    @FXML
    public PasswordField passwordField;
    @FXML
    public Button registerButton;
    @FXML
    public PasswordField registerPasswordRepeatField;
    @FXML
    public PasswordField registerPasswordField;
    @FXML
    public TextField registerNameField;
    @FXML
    public AnchorPane loadingPane;
    @FXML
    public TabPane loginTabs;
    @FXML
    public AnchorPane loggedPane;
    @FXML
    public Label loggedAsLabel;
    @FXML
    public Button logoutButton;
    @FXML
    public Button loggedPlayButton;
    @FXML
    public ImageView headImage;
    @Getter
    private static LauncherController instance;
    public AnchorPane mainContainer;
    public ImageView closeButton;

    private static final Image CLOSE_IMAGE_HOVER = new Image("/assets/close_hover.png");
    private static final Image CLOSE_IMAGE = new Image("/assets/close.png");
    public CheckComboBox<RaisemonRepo.RepoFile> optionalMods;
    public Slider ramSlider;
    public ImageView logo;
    private String token;
    private String name;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        instance = this;
        EffectUtilities.makeDraggable(Launcher.getInstance().getPrimaryStage(), mainContainer);
        EffectUtilities.makeDraggable(Launcher.getInstance().getPrimaryStage(), logo);
        statusLabel.setTextAlignment(TextAlignment.JUSTIFY);
        statusLabel.setWrapText(true);
        final Config.Settings settings = Launcher.getInstance().getConfig().getSettings();
        if (settings.getTokenData() != null ) {

            final LoginResponse loginResponse = RaisemonAPI.refreshToken(settings.getTokenData().getRefreshToken());
            if (loginResponse == null) {
                loadingPane.setVisible(false);
                loginTabs.setVisible(true);
            } else {
                this.token = loginResponse.getAccess_token();
                this.name = loginResponse.getName();
                settings.getTokenData().setRefreshToken(loginResponse.getRefresh_token());
                settings.getTokenData().setName(loginResponse.getName());
                updateSkin(settings.getTokenData().getName());
                loggedAsLabel.setText(settings.getTokenData().getName());
                loggedPane.setVisible(true);
                loadingPane.setVisible(false);
            }
        } else {
            loadingPane.setVisible(false);
            loginTabs.setVisible(true);
        }
        ramSlider.setValue(settings.getRam()/1024d);

        playButton.setOnMouseClicked(event -> {
            loadingPane.setVisible(true);
            loginTabs.setVisible(false);
            final LoginResponse login = RaisemonAPI.login(nicknameField.getText(), passwordField.getText());
            if (login == null) {
                Launcher.getInstance().showDialog("Błąd logowania!", "Podane dane logowania są nieprawidłowe lub nastąpił problem komunikacji ze stroną serwera.", Alert.AlertType.ERROR);

                loadingPane.setVisible(false);
                loginTabs.setVisible(true);
                return;
            }
            updateSkin(login.getName());
            if (savePasswordCheckBox.isSelected()) {
                Launcher.getInstance().getConfig().getSettings().setTokenData(new Config.LoginTokenData(login.getRefresh_token(), login.getName()));
            }else{
                Launcher.getInstance().getConfig().getSettings().setTokenData(null);
            }
            this.token = login.getAccess_token();
            this.name = login.getName();
            Launcher.getInstance().getConfig().save();
            loggedAsLabel.setText(login.getName());
            loggedPane.setVisible(true);
            loadingPane.setVisible(false);
        });
        loggedPlayButton.setOnMouseClicked(event -> {
            loggedPlayButton.setDisable(true);
            logoutButton.setDisable(true);
            Launcher.getInstance().getConfig().getSettings().setRam((int) (ramSlider.getValue()*1024));
            Launcher.getInstance().getConfig().save();
            final Thread thread = new Thread(() -> {
                Launcher.getInstance().startGame(token, name);
            });
            thread.setDaemon(false);
            thread.start();
        });
        registerButton.setOnMouseClicked(event -> {
            loadingPane.setVisible(true);
            loginTabs.setVisible(false);
            final RaisemonAPI.RegisterResponse register = RaisemonAPI.register(registerNameField.getText(), registerPasswordField.getText(), registerPasswordRepeatField.getText());
            assert register != null;
            if (!register.isSuccess()) {
                Launcher.getInstance().showDialog("Błąd rejestracji!", "Musisz poprawić następujące błędy:\r\n" + register.getErrors().stream().collect(Collectors.joining("\r\n")), Alert.AlertType.WARNING);
                loadingPane.setVisible(false);
                loginTabs.setVisible(true);
                return;
            }
            registerNameField.setText("");
            registerPasswordRepeatField.setText("");
            registerPasswordField.setText("");
            Launcher.getInstance().showDialog("Sukces!", "Udało się zarejestrować konto!", Alert.AlertType.CONFIRMATION);
            loadingPane.setVisible(false);
            loginTabs.setVisible(true);
        });

        logoutButton.setOnMouseClicked(event -> {
            RaisemonAPI.logout(token);
            Launcher.getInstance().getConfig().getSettings().setTokenData(null);
            Launcher.getInstance().getConfig().save();
            loggedPane.setVisible(false);
            loginTabs.setVisible(true);
        });

        closeButton.setOnMouseClicked(event -> {
            Launcher.getInstance().getPrimaryStage().close();
            Platform.exit();
            System.exit(0);
        });
        closeButton.hoverProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) closeButton.setImage(CLOSE_IMAGE_HOVER);
            else closeButton.setImage(CLOSE_IMAGE);
        });
        Platform.runLater(() -> {
            try {
                final RaisemonRepo raisemonRepo = GSON.fromJson(Unirest.get("http://cdn.raisemon.pl/repo.json").asString().getBody(), RaisemonRepo.class);
                if(!raisemonRepo.getLauncher().getVersion().equalsIgnoreCase(Launcher.getInstance().getVersion())){
                    Launcher.getInstance().showDialog("Aktualizacja!", "Dostępna do pobrania jest nowa wersja launchera.\nLink: " + raisemonRepo.getLauncher().getUrl(), Alert.AlertType.INFORMATION);
                }
                Launcher.getInstance().setRepo(raisemonRepo);
                raisemonRepo.getDirectories().entrySet().stream()
                        .flatMap((Function<Map.Entry<String, RaisemonRepo.RepoDirectory>, Stream<RaisemonRepo.RepoFile>>) stringRepoDirectoryEntry -> Arrays.stream(stringRepoDirectoryEntry.getValue().getFiles()))
                        .filter(file -> file.getOptional() != null).forEach(e -> {
                            optionalMods.getItems().add(e);
                            if(Launcher.getInstance().getConfig().getSettings().getOptionalMods().contains(e.getOptional().getId())){
                                optionalMods.getCheckModel().check(e);
                            }
                });
                Launcher.getInstance().getConfig().getSettings().getOptionalMods().removeIf(mod -> optionalMods.getItems().stream().noneMatch(rf -> rf.getOptional().getId().equals(mod)));
            } catch (UnirestException e) {
                e.printStackTrace();
                Launcher.getSentry().sendException(e);
            }
        });

        optionalMods.getCheckModel().getCheckedItems().addListener((ListChangeListener<RaisemonRepo.RepoFile>) c -> {
            while (c.next()) {
                for (RaisemonRepo.RepoFile repoFile : c.getRemoved()) {
                    Launcher.getInstance().getConfig().getSettings().getOptionalMods().remove(repoFile.getOptional().getId());
                }
                top:
                for (RaisemonRepo.RepoFile repoFile : c.getAddedSubList()) {
                    for (String collide : repoFile.getOptional().getCollides()) {
                        if(Launcher.getInstance().getConfig().getSettings().getOptionalMods().contains(collide)){
                            final RaisemonRepo.RepoFile optionalById = Launcher.getInstance().getRepo().getOptionalById(collide);
                            if(optionalById==null){
                                continue;
                            }
                            Launcher.getInstance().showDialog("Wymagany mod", "Wybrany mod koliduje z " + optionalById.getName(), Alert.AlertType.ERROR);
                            Platform.runLater(()->{
                                optionalMods.getCheckModel().clearCheck(repoFile);
                            });
                            break top;
                        }
                    }
                    for (String s : repoFile.getOptional().getRequire()) {
                        if (!Launcher.getInstance().getConfig().getSettings().getOptionalMods().contains(s)) {
                            final RaisemonRepo.RepoFile optionalById = Launcher.getInstance().getRepo().getOptionalById(s);
                            if(optionalById==null){
                                continue;
                            }
                            for (String collide : optionalById.getOptional().getCollides()) {
                                if(Launcher.getInstance().getConfig().getSettings().getOptionalMods().contains(collide)){
                                    Launcher.getInstance().showDialog("Wymagany mod", "Wybrany mod koliduje z " + Launcher.getInstance().getRepo().getOptionalById(collide).getName(), Alert.AlertType.ERROR);
                                    Platform.runLater(()->{
                                        optionalMods.getCheckModel().clearCheck(repoFile);
                                    });
                                    break top;
                                }
                            }
                            optionalMods.getCheckModel().check(optionalById);
                        }
                    }
                    Launcher.getInstance().getConfig().getSettings().getOptionalMods().add(repoFile.getOptional().getId());
                }
            }
            Launcher.getInstance().getConfig().save();
        });

    }

    private void updateSkin(String name) {
        headImage.setImage(new Image("https://raisemon.pl/api/render/head/" + name + ".png"));

    }

    public void update(double i, String s) {
        Platform.runLater(() -> {
            progressBar.setProgress(i);
            statusLabel.setText(s);
        });
    }
}
