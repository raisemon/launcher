package pl.raisemon.launcher;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.sentry.Sentry;
import io.sentry.SentryClient;
import io.sentry.SentryClientFactory;
import io.sentry.context.Context;
import io.sentry.event.UserBuilder;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.Getter;
import lombok.Setter;
import pl.raisemon.launcher.data.Assets;
import pl.raisemon.launcher.data.Config;
import pl.raisemon.launcher.data.MinecraftVersion;
import pl.raisemon.launcher.data.RaisemonRepo;
import pl.raisemon.launcher.downloader.DownloadTask;
import pl.raisemon.launcher.downloader.Downloader;
import pl.raisemon.launcher.downloader.HashingMethod;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Launcher extends Application {

    @Getter
    public String version = "2.0";

    public static final Gson GSON = new Gson();
    @Getter
    private static SentryClient sentry;
    @Getter
    private Context context;
    @Getter
    private static Launcher instance;
    @Getter
    private File launcherDir = new File(System.getProperty("user.home"), ".raisemon");
    @Getter
    private Config config;
    @Getter
    private Stage primaryStage;
    @Getter
    @Setter
    private RaisemonRepo repo;


    public static void main(String... args) {
        System.setProperty("java.net.preferIPv4Stack" , "true");
        Sentry.init();
        sentry = SentryClientFactory.sentryClient();
        Launcher.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        instance = this;
        this.primaryStage = primaryStage;
        primaryStage.getIcons().add(new Image("/assets/icon.png"));

        context = sentry.getContext();
        launcherDir.mkdirs();
        this.config = new Config(new File(launcherDir, "config.json"));
        Parent load;
        if(this.config.getSettings().isLowProfile()){
            load = FXMLLoader.load(Launcher.class.getResource("/launcher_low.fxml"));
        }else {
            this.config.getSettings().setLowProfile(true);
            this.config.save();
            load = FXMLLoader.load(Launcher.class.getResource("/launcher.fxml"));
            this.config.getSettings().setLowProfile(false);
            this.config.save();
        }

        final Scene scene = new Scene(load);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        scene.setFill(Color.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.setTitle("RaiseMon.pl Launcher");
        primaryStage.show();

    }

    public void startGame(String token, String name) {
        context.setUser(new UserBuilder().setUsername(name).build());

        LauncherController.getInstance().update(-1, "Pobieranie informacji o wersji forge!");
        MinecraftVersion forgeVersion = null;
        try {
            forgeVersion = GSON.fromJson(Unirest.get("http://cdn.raisemon.pl/version.json").asString().getBody(), MinecraftVersion.class);
        } catch (UnirestException e) {
            e.printStackTrace();
            sentry.sendException(e);
            return;
        }
        if(forgeVersion.getInheritsFrom() != null){
            LauncherController.getInstance().update(-1, "Pobieranie informacji o wersji minecrafta!");
            try {
                for (JsonElement versions : GSON.fromJson(Unirest.get("https://launchermeta.mojang.com/mc/game/version_manifest.json").asString().getBody(), JsonObject.class)
                        .get("versions").getAsJsonArray()) {
                    if(versions.getAsJsonObject().get("id").getAsString().equalsIgnoreCase(forgeVersion.getInheritsFrom())){
                        try {
                            forgeVersion.merge(GSON.fromJson(Unirest.get(versions.getAsJsonObject().get("url").getAsString()).asString().getBody(), MinecraftVersion.class));
                        } catch (UnirestException e) {
                            e.printStackTrace();
                            sentry.sendException(e);
                        }

                        break;
                    }
                }
            } catch (UnirestException e) {
                e.printStackTrace();
                sentry.sendException(e);
            }
        }
        LauncherController.getInstance().update(-1, "Inicjalizowanie wątku pobierania!");
        System.out.println("Creating downloader");
        final Downloader downloader = new Downloader();
        for (MinecraftVersion.MinecraftLibrary library : forgeVersion.getLibraries()) {
            System.out.println("new library task " + library.getName());
            downloader.addTasks(DownloadTask.from(library));
        }
        System.out.println("loading assets");
        Assets assets = null;
        final String url = forgeVersion.getAssetIndex().getUrl();
        try {
            assets = GSON.fromJson(Unirest.get(url).asString().getBody(), Assets.class);
        } catch (UnirestException e) {
            e.printStackTrace();
            sentry.sendException(e);
        }
        System.out.println("adding assets indexes task");
        final String[] splittedAssetsUrl = url.split("/");
        downloader.addTask(new DownloadTask<Integer>("Assets indexes", "assets/indexes/" +splittedAssetsUrl[splittedAssetsUrl.length-1], url, HashingMethod.SIZE, null, true));
        for (Assets.Asset value : assets.getObjects().values()) {
            downloader.addTasks(DownloadTask.from(value));
        }

        LauncherController.getInstance().update(-1, "Pobieranie danych na temat wymaganych modów!");

        repo.getDirectories().forEach((dirName, dir)->{
            final File file1 = new File(getLauncherDir(), dirName);
            if(!file1.exists()){
                file1.mkdirs();
            }
            final List<String> strings = new ArrayList<>(Arrays.asList(file1.list()));
            for (RaisemonRepo.RepoFile file : dir.getFiles()) {
                if(file.getOptional() == null ||
                        Launcher.getInstance().getConfig().getSettings().getOptionalMods()
                                .contains(file.getOptional().getId())){
                    strings.remove(file.getPath());
                    downloader.addTask(new DownloadTask<>(file.getName(), dirName + "/" + file.getPath(), file.getUrl(), HashingMethod.CRC32, file.getHash(), false));
                }
            }
            if(dir.isClear()){
                for (String string : strings) {
                    new File(file1, string).delete();
                }
            }
        });

        downloader.addTask(new DownloadTask<>("Minecraft", "version.jar", forgeVersion.getDownloads().get("client").getUrl(), HashingMethod.SHA1, forgeVersion.getDownloads().get("client").getSha1(), false));
        downloader.addTask(new DownloadTask<>("Agent", "agent.jar", repo.getLauncher().getAgentUrl(), HashingMethod.CRC32, repo.getLauncher().getAgentHash(), false));

        LauncherController.getInstance().update(-1, "Rozpoczynanie pobierania!");
        final MinecraftLauncher minecraftLauncher = new MinecraftLauncher(forgeVersion);
        minecraftLauncher.setRam(config.getSettings().getRam());
        minecraftLauncher.setToken(token);
        minecraftLauncher.setName(name);
        downloader.setOnFinishListener(minecraftLauncher::launch);
        downloader.start();

    }

    public void showDialog(String title, String content, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setContentText(content);
        alert.showAndWait();
    }

}
